package alarm.alarm;

import android.app.Activity;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;
import java.util.Map;

public class myActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        Uri ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        final Ringtone ringtoneSound = RingtoneManager.getRingtone(getApplicationContext(), ringtoneUri);

        if (ringtoneSound != null) {
            //ringtoneSound.play();
        }

        Ringtone ringtone=null;
        Map<String, String> ringtoneMap = getNotifications();

        RingtoneManager ringtoneManager = new RingtoneManager(this);
        ringtoneManager.setType(RingtoneManager.TYPE_RINGTONE);
        Cursor cursor = ringtoneManager.getCursor();
        while (cursor.moveToNext()) {
            Log.i("AAA", cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX));
            Log.i("BBB", cursor.getString(RingtoneManager.URI_COLUMN_INDEX));
            Uri ring = Uri.parse(cursor.getString(RingtoneManager.URI_COLUMN_INDEX));
            ringtone=RingtoneManager.getRingtone(getApplicationContext(),ring);

        }

        if (ringtone != null) {
            ringtone.play();
        }


        Button button = (Button) findViewById(R.id.the_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ringtoneSound.stop();
            }
        });

    }

    public Map<String, String> getNotifications() {
        RingtoneManager manager = new RingtoneManager(this);
        manager.setType(RingtoneManager.TYPE_RINGTONE);
        Cursor cursor = manager.getCursor();

        Map<String, String> list = new HashMap<>();
        while (cursor.moveToNext()) {
            String notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
            String notificationUri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);

            list.put(notificationTitle, notificationUri);
        }

        return list;
    }

}
