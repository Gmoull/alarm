package alarm.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

import android.app.Activity;

import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button buttonStart = (Button)findViewById(R.id.the_button);
        buttonStart.setOnClickListener(new Button.OnClickListener(){

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(getBaseContext(),
                        myReceiver.class);

                PendingIntent pendingIntent
                        = PendingIntent.getBroadcast(getBaseContext(),
                        0, myIntent, 0);

                AlarmManager alarmManager
                        = (AlarmManager)getSystemService(ALARM_SERVICE);
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.add(Calendar.SECOND, 10);
                long interval = 6 * 1000; //
                //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                    //    calendar.getTimeInMillis(), interval, pendingIntent);
                alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis()+interval,pendingIntent);
               // finish();
            }});
    }

}


